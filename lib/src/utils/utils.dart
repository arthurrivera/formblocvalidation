import 'package:flutter/material.dart';

bool isNum(String s) {
  if (s.isEmpty) return false;
  final n = num.tryParse(s);

  return (n == null) ? false : true;
}

mostrarAlerta(BuildContext context, String mensaje) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text('Información Incorrecta'),
        content: Text(mensaje),
        actions: [
          FlatButton(onPressed: () => Navigator.pop(context), child: Text('Ok'))
        ],
      );
    },
  );
}
