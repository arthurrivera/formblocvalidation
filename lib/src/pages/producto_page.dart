import 'package:flutter/material.dart';
import 'package:form_validation/src/bloc/productos_bloc.dart';
import 'package:form_validation/src/bloc/provider.dart';
import 'package:form_validation/src/models/producto_model.dart';
import 'package:form_validation/src/utils/utils.dart' as utils;
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class ProductoPage extends StatefulWidget {
  @override
  _ProductoPageState createState() => _ProductoPageState();
}

class _ProductoPageState extends State<ProductoPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final picker = ImagePicker();
  File foto;

  ProductosBloc productosBloc;
  ProductoModel producto = ProductoModel();
  bool _guardando = false;

  @override
  Widget build(BuildContext context) {
    productosBloc = Provider.productosBloc(context);
    final ProductoModel prodData = ModalRoute.of(context).settings.arguments;
    if (prodData != null) producto = prodData;
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Producto'),
        actions: [
          IconButton(
              icon: Icon(Icons.photo_size_select_actual),
              onPressed: _seleccionarFoto),
          IconButton(icon: Icon(Icons.camera), onPressed: _tomarFoto)
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
              key: formKey,
              child: Column(
                children: [
                  _mostrarFoto(),
                  _crearNombre(),
                  _crearPrecio(),
                  _crearDisponlible(),
                  _crearBoton(context)
                ],
              )),
        ),
      ),
    );
  }

  Widget _crearNombre() {
    return TextFormField(
      initialValue: producto.titulo,
      decoration: InputDecoration(labelText: 'Producto'),
      textCapitalization: TextCapitalization.words,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese el nombre del producto';
        } else {
          return null;
        }
      },
      onSaved: (newValue) => producto.titulo = newValue,
    );
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        foto = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Widget _crearPrecio() {
    return TextFormField(
      initialValue: producto.valor.toString(),
      decoration: InputDecoration(labelText: 'Precio'),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      validator: (value) {
        if (utils.isNum(value)) {
          return null;
        } else {
          return 'Sólo números';
        }
      },
      onSaved: (newValue) => producto.valor = double.parse(newValue),
    );
  }

  Widget _crearBoton(BuildContext context) {
    return RaisedButton.icon(
      onPressed: _guardando ? null : _submit,
      textColor: Colors.white,
      label: Text('Guardar'),
      icon: Icon(Icons.save),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      color: Theme.of(context).primaryColor,
    );
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;
    print('TODO Ok');
    formKey.currentState.save();
    _guardando = true;
    setState(() {});
    if (foto != null) {
      producto.photoUrl = await productosBloc.subirFoto(foto);
    }
    if (producto.id == null)
      productosBloc.agregarProducto(producto);
    else {
      productosBloc.editarProducto(producto);
    }
    mostrarSnackbar('Registro Guardado');
    Navigator.pop(context);
  }

  Widget _crearDisponlible() {
    return SwitchListTile(
        value: producto.disponible,
        title: Text('Disponible'),
        activeColor: Theme.of(context).primaryColor,
        onChanged: (value) => setState(() {
              producto.disponible = value;
            }));
  }

  void mostrarSnackbar(String mensaje) {
    final snackbar = SnackBar(
      content: Text(mensaje),
      duration: Duration(milliseconds: 1500),
    );
    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  _seleccionarFoto() async {
    _procesarImagen(ImageSource.gallery);
  }

  Widget _mostrarFoto() {
    if (producto.photoUrl != null) {
      return FadeInImage(
          placeholder: AssetImage('assets/jar-loading.gif'),
          height: 300.0,
          fit: BoxFit.contain,
          image: NetworkImage(producto.photoUrl));
    } else {
      return Image(
        image: AssetImage(foto?.path ?? 'assets/no-image.png'),
        fit: BoxFit.cover,
        height: 300.0,
      );
    }
  }

  void _tomarFoto() async {
    _procesarImagen(ImageSource.camera);
  }

  _procesarImagen(ImageSource origen) async {
    final file = await picker.getImage(source: origen);

    if (foto != null) producto.photoUrl = null;

    if (file != null) {
      foto = File(file.path);
    } else {
      print('No image selected.');
    }
    setState(() {});
  }
}
